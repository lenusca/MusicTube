"""Music URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from app import views
urlpatterns = [
    #path('', views.index),
    path('artists/', views.artists),
    path('genres/', views.genres),
    path('albums/', views.albums),
    path('', views.index),
    path('artists_list/', views.artists_list),
    path('musics/', views.musics),
    path('albums_list/', views.albums_list),
    path('genres_list/', views.genres_list),
    path('music_details/', views.detailsMusic),
    path('album_details/', views.detailsAlbum),
    path('album_details2/', views.detailsAlbum2),
    path('genre_details2/', views.detailsGenres2),
    path('artist_details/', views.detailsArtists),
    path('playlist/', views.playlist),
    path('addmusic/', views.addMusic),
    path('removemusic/', views.removeMusic),
    path('add_artist/', views.addArtist),
    path('add_genre/', views.addGenre),
    path('add_album/', views.addAlbum),
    path('removeartist/', views.removeArtist),
    path('removealbum/', views.removeAlbum),
    path('removegenre/', views.removeGenre),
]
