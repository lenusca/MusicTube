from django.shortcuts import render, redirect
from django.template.defaulttags import register
import json
from s4api.graphdb_api import GraphDBApi
from s4api.swagger import ApiClient
import requests
import secrets

def index(request):
    wiki_id = []
    wiki_id2 = []
    wiki = []
    wiki2 = []
    a = dict()
    a_photo = dict()
    photo = []
    photo2 = []
    name = []
    name2 = []
    s = dict()
    s_photo = dict()
    url = 'https://query.wikidata.org/sparql'
    query = '''
                   SELECT DISTINCT ?photo ?item ?name WHERE {
                    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                    ?item wdt:P31 wd:Q215380;
                          wdt:P18 ?photo;
                          wdt:P1448 ?name
                   }
                   limit 10
               '''
    r = requests.get(url, params={'format': 'json', 'query': query})
    data = r.json()
    for e in data['results']['bindings']:
        wiki.append(e['item']['value'])
        a[e['item']['value']] = e['name']['value']
        a_photo[e['item']['value']] = e['photo']['value']

    url = 'https://query.wikidata.org/sparql'
    query = '''
                   SELECT DISTINCT ?item ?photo ?title WHERE {
                   SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                   ?item wdt:P31 wd:Q482994 ;
                         wdt:P18 ?photo;
                         wdt:P1476 ?title
                   }
                    limit 10
              '''
    r = requests.get(url, params={'format': 'json', 'query': query})
    data = r.json()
    for e in data['results']['bindings']:
        wiki2.append(e['item']['value'])
        s_photo[e['item']['value']] = e['photo']['value']
        s[e['item']['value']] = e['title']['value']

    for i in range(0, 3):
        id = secrets.choice(wiki)
        id2 = secrets.choice(wiki2)
        wiki_id.append(id)
        wiki_id2.append(id2)
        photo.append(a_photo[id])
        photo2.append(s_photo[id2])
        name.append(a[id])
        name2.append(s[id2])

    tparams = {
        'a1name' : name[0],
        'a2name' : name[1],
        'a3name' : name[2],
        'a1photo' : photo[0],
        'a2photo': photo[1],
        'a3photo': photo[2],
        'a1wiki': wiki_id[0],
        'a2wiki': wiki_id[1],
        'a3wiki': wiki_id[2],


        'a4name': name2[0],
        'a5name': name2[1],
        'a6name': name2[2],
        'a4photo': photo2[0],
        'a5photo': photo2[1],
        'a6photo': photo2[2],
        'a4wiki': wiki_id2[0],
        'a5wiki': wiki_id2[1],
        'a6wiki': wiki_id2[2],
    }
    return render(request, 'index.html', tparams)

def artists(request):
    photos = dict()
    a_name = dict()

    url = 'https://query.wikidata.org/sparql'
    query = '''
               SELECT DISTINCT ?item ?photo ?itemLabel WHERE {
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                
                ?item wdt:P31 wd:Q215380;
                      wdt:P18 ?photo.
               }limit 200
           '''
    r = requests.get(url, params={'format': 'json', 'query': query})
    data = r.json()
    for e in data['results']['bindings']:
        photos[e['item']['value']] = e['photo']['value']
        a_name[e['item']['value']] = e['itemLabel']['value']
    tparams = {
        'photos': photos,
        'artists': a_name,
    }
    return render(request, 'artists.html', tparams)

def artists_list(request):
    artist = dict()
    endpoint = "http://localhost:7200"
    repo_name = "Musica"
    client = ApiClient(endpoint=endpoint)
    accessor = GraphDBApi(client)
    query = """
            PREFIX mus:<http://www.music.pt/preds/>
            SELECT DISTINCT ?artist_id ?artist ?wiki_id 
            WHERE{
                ?artist_id mus:artist ?artist.
                ?artist_id mus:wikidataid ?wiki_id.
            }limit 100
    """
    if 'Letter' in request.GET:
        if request.GET['Letter'] == 'ALL':
            query = """
                        PREFIX mus:<http://www.music.pt/preds/>
                        SELECT DISTINCT ?artist_id ?artist ?wiki_id 
                        WHERE{
                            ?artist_id mus:artist ?artist.
                            ?artist_id mus:wikidataid ?wiki_id.
                        }
                """
        else:
            query = '''
                        PREFIX mus:<http://www.music.pt/preds/>
                        SELECT DISTINCT ?artist_id ?artist ?wiki_id 
                        WHERE{
                            ?artist_id mus:artist ?artist.
                            ?artist_id mus:wikidataid ?wiki_id.
                            FILTER regex(?artist, "''' +"^"+ request.GET['Letter']+ '''", "i")
                        }
                '''

    if 'Order' in request.GET:
        if request.GET['Order'] == 'Artists':
            query = """
                PREFIX mus:<http://www.music.pt/preds/>
                SELECT DISTINCT ?artist_id ?artist ?wiki_id 
                WHERE{
                    ?artist_id mus:artist ?artist.
                    ?artist_id mus:wikidataid ?wiki_id.
                }
                ORDER BY STR(?artist) limit 100
            """

        if request.GET['Order'] == 'ZArtists':
            query = """
                PREFIX mus:<http://www.music.pt/preds/>
                SELECT DISTINCT ?artist_id ?artist ?wiki_id 
                WHERE{
                    ?artist_id mus:artist ?artist.
                    ?artist_id mus:wikidataid ?wiki_id.
                }
                ORDER BY desc(STR(?artist)) limit 100
            """
    payload_query = {"query": query}
    res = accessor.sparql_select(body=payload_query,
                                 repo_name=repo_name)
    res = json.loads(res)
    for e in res['results']['bindings']:
        artist[e['wiki_id']['value']] = e['artist']['value']
    tparams = {
        'artist': artist
    }
    return render(request, 'listartists.html', tparams)

def genres(request):
    photos = dict()
    g_name = dict()

    url = 'https://query.wikidata.org/sparql'
    query = '''
               SELECT DISTINCT ?photo ?item ?name WHERE {
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                ?item wdt:P31 wd:Q188451;
                      wdt:P18 ?photo;
                      wdt:P373 ?name
                      
                }           
           '''
    r = requests.get(url, params={'format': 'json', 'query': query})
    data = r.json()
    for e in data['results']['bindings']:
        photos[e['item']['value']] = e['photo']['value']
        g_name[e['item']['value']] = e['name']['value']
    tparams = {
        'photos': photos,
        'generos': g_name,
    }
    return render(request, 'genres.html', tparams)

def genres_list(request):
    genres = dict()
    endpoint = "http://localhost:7200"
    repo_name = "Musica"
    client = ApiClient(endpoint=endpoint)
    accessor = GraphDBApi(client)
    query = """
            PREFIX mus:<http://www.music.pt/preds/>
                SELECT DISTINCT ?genre_id ?genre 
                WHERE{
                    ?artist_id mus:genre ?genre_id.
    				?genre_id mus:name ?genre.
                }
    """
    if 'Letter' in request.GET:
        if request.GET['Letter'] == 'ALL':
            query = """
                        PREFIX mus:<http://www.music.pt/preds/>
                            SELECT DISTINCT ?genre_id ?genre 
                            WHERE{
                                ?artist_id mus:genre ?genre_id.
                				?genre_id mus:name ?genre.
                            }
                """
        else:
            query = '''
                PREFIX mus:<http://www.music.pt/preds/>
                SELECT DISTINCT ?genre_id ?genre
                WHERE{
                    ?artist_id mus:genre ?genre_id.
                    ?genre_id mus:name ?genre.
                    FILTER regex(?genre, "''' +"^"+ request.GET['Letter']+ '''", "i")
                }
            '''
    if 'Order' in request.GET:
        if request.GET['Order'] == 'Genres':
            query = """
                PREFIX mus:<http://www.music.pt/preds/>
                SELECT DISTINCT ?genre_id ?genre
                WHERE{
                    ?artist_id mus:genre ?genre_id.
                    ?genre_id mus:name ?genre.
                }
                ORDER BY STR(?genre)
            """

        if request.GET['Order'] == 'ZGenres':
            query = """
                PREFIX mus:<http://www.music.pt/preds/>
                SELECT DISTINCT ?genre_id ?genre
                WHERE{
                    ?artist_id mus:genre ?genre_id.
                    ?genre_id mus:name ?genre.
                }
                ORDER BY desc(STR(?genre))
            """
    payload_query = {"query": query}
    res = accessor.sparql_select(body=payload_query,
                                 repo_name=repo_name)
    res = json.loads(res)
    for e in res['results']['bindings']:
        genres[e['genre_id']['value']] = e['genre']['value']
    tparams = {
        'genres': genres
    }
    return render(request, 'listgenres.html', tparams)

def albums(request):
    photos = dict()
    al_name = dict()

    url = 'https://query.wikidata.org/sparql'
    query = '''
                SELECT DISTINCT ?item ?photo ?title WHERE {
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                ?item wdt:P31 wd:Q482994 ;
                      wdt:P18 ?photo;
                      wdt:P1476 ?title
                }
                      
           '''
    r = requests.get(url, params={'format': 'json', 'query': query})
    data = r.json()
    for e in data['results']['bindings']:
        photos[e['item']['value']] = e['photo']['value']
        al_name[e['item']['value']] = e['title']['value']
    tparams = {
        'photos': photos,
        'albums': al_name,
    }
    return render(request, 'albums.html', tparams)

def albums_list(request):
    album = dict()
    endpoint = "http://localhost:7200"
    repo_name = "Musica"
    client = ApiClient(endpoint=endpoint)
    accessor = GraphDBApi(client)
    query = """
            PREFIX mus:<http://www.music.pt/preds/>
            SELECT DISTINCT ?album_id ?album
            WHERE{
                ?artist_id mus:album ?album_id.
                ?album_id mus:name ?album.
            }
    """
    if 'Letter' in request.GET:
        if request.GET['Letter'] == 'ALL':
            query = """
                        PREFIX mus:<http://www.music.pt/preds/>
                        SELECT DISTINCT ?album_id ?album
                        WHERE{
                            ?artist_id mus:album ?album_id.
                            ?album_id mus:name ?album.
                        }
                """

        else:
            query = '''
                    PREFIX mus:<http://www.music.pt/preds/>
                    SELECT DISTINCT ?album_id ?album
                    WHERE{
                        ?artist_id mus:album ?album_id.
                        ?album_id mus:name ?album.
                        FILTER regex(?album,"''' +"^"+ request.GET['Letter']+ '''", "i")
                }
            '''
    if 'Order' in request.GET:
        if request.GET['Order'] == 'Albums':
            query = """
                PREFIX mus:<http://www.music.pt/preds/>
                SELECT DISTINCT ?album_id ?album
                WHERE{
                    ?artist_id mus:album ?album_id.
                    ?album_id mus:name ?album.
                }
                ORDER BY STR(?album)
            """
        if request.GET['Order'] == 'ZAlbums':
            query = """
                PREFIX mus:<http://www.music.pt/preds/>
                SELECT DISTINCT ?album_id ?album
                WHERE{
                    ?artist_id mus:album ?album_id.
                    ?album_id mus:name ?album.
                }
                ORDER BY desc(STR(?album))
            """
    payload_query = {"query": query}
    res = accessor.sparql_select(body=payload_query,
                                 repo_name=repo_name)
    res = json.loads(res)
    for e in res['results']['bindings']:
        album[e['album_id']['value']] = e['album']['value']
    tparams = {
        'album': album
    }
    return render(request, 'listalbuns.html', tparams)

def musics(request):
    music = dict()
    name = dict()
    url = 'https://query.wikidata.org/sparql'

    if 'Letter' in request.GET:
        if request.GET['Letter'] == 'ALL':
            query = '''
                           SELECT DISTINCT ?item ?music ?name WHERE {
                          {
                                        SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                                        ?item wdt:P31 wd:Q134556 ;
                                              wdt:P1476 ?music;
                                              wdt:P175 ?artist.
                                        ?artist wdt:P1477 ?name.
                          }
                          UNION
                          {
                                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                                        ?item wdt:P31 wd:Q134556 ;
                                              wdt:P1476 ?music;
                                              wdt:P175 ?artist.
                                        ?artist wdt:P1448 ?name
                          }

                         }

                       '''
            r = requests.get(url, params={'format': 'json', 'query': query})
            data = r.json()
            for e in data['results']['bindings']:
                music[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['music']['value']
                name[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['name']['value']

        else:
            query = '''
                    SELECT DISTINCT ?item ?music ?name WHERE {
                      {
                                    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                                    ?item wdt:P31 wd:Q134556 ;
                                          wdt:P1476 ?music;
                                          wdt:P175 ?artist.
                                    ?artist wdt:P1477 ?name.
                      }
                      UNION
                      {
                            SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                                    ?item wdt:P31 wd:Q134556 ;
                                          wdt:P1476 ?music;
                                          wdt:P175 ?artist.
                                    ?artist wdt:P1448 ?name
                      }
                      FILTER regex(?music, "''' +"^"+ request.GET['Letter']+ '''", "i")
                    }
                    '''
            r = requests.get(url, params={'format': 'json', 'query': query})
            data = r.json()
            for e in data['results']['bindings']:
                music[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['music']['value']
                name[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['name']['value']

    if 'Order' in request.GET:
        if request.GET['Order'] == 'Musics':
            query = '''
                  SELECT DISTINCT ?item ?music ?name WHERE {
                  {
                                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                                ?item wdt:P31 wd:Q134556 ;
                                      wdt:P1476 ?music;
                                      wdt:P175 ?artist.
                                ?artist wdt:P1477 ?name.
                  }
                  UNION
                  {
                        SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                                ?item wdt:P31 wd:Q134556 ;
                                      wdt:P1476 ?music;
                                      wdt:P175 ?artist.
                                ?artist wdt:P1448 ?name
                  }
                }ORDER BY STR(?music)
            '''
            r = requests.get(url, params={'format': 'json', 'query': query})
            data = r.json()
            for e in data['results']['bindings']:
                music[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['music']['value']
                name[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['name']['value']

        if request.GET['Order'] == 'ZMusics':
            query = '''
                  SELECT DISTINCT ?item ?music ?name WHERE {
                  {
                                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                                ?item wdt:P31 wd:Q134556 ;
                                      wdt:P1476 ?music;
                                      wdt:P175 ?artist.
                                ?artist wdt:P1477 ?name.
                  }
                  UNION
                  {
                        SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                                ?item wdt:P31 wd:Q134556 ;
                                      wdt:P1476 ?music;
                                      wdt:P175 ?artist.
                                ?artist wdt:P1448 ?name
                  }
                }ORDER BY desc(STR(?music))
            '''
            r = requests.get(url, params={'format': 'json', 'query': query})
            data = r.json()
            for e in data['results']['bindings']:
                music[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['music']['value']
                name[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['name']['value']

        if request.GET['Order'] == 'Artist':
            query = '''
                     SELECT DISTINCT ?item ?music ?name WHERE {
                      {
                                    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                                    ?item wdt:P31 wd:Q134556 ;
                                          wdt:P1476 ?music;
                                          wdt:P175 ?artist.
                                    ?artist wdt:P1477 ?name.
                      }
                      UNION
                      {
                            SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                                    ?item wdt:P31 wd:Q134556 ;
                                          wdt:P1476 ?music;
                                          wdt:P175 ?artist.
                                    ?artist wdt:P1448 ?name
                      }
                    }ORDER BY STR(?name)
                    
                        '''
            r = requests.get(url, params={'format': 'json', 'query': query})
            data = r.json()
            for e in data['results']['bindings']:
                music[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['music']['value']
                name[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['name']['value']

        if request.GET['Order'] == 'ZArtist':
            query = '''
                     SELECT DISTINCT ?item ?music ?name WHERE {
                      {
                                    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                                    ?item wdt:P31 wd:Q134556 ;
                                          wdt:P1476 ?music;
                                          wdt:P175 ?artist.
                                    ?artist wdt:P1477 ?name.
                      }
                      UNION
                      {
                            SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                                    ?item wdt:P31 wd:Q134556 ;
                                          wdt:P1476 ?music;
                                          wdt:P175 ?artist.
                                    ?artist wdt:P1448 ?name
                      }
                    }ORDER BY desc(STR(?name))

                        '''
            r = requests.get(url, params={'format': 'json', 'query': query})
            data = r.json()
            for e in data['results']['bindings']:
                music[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['music']['value']
                name[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['name']['value']

        if request.GET['Order'] == 'Recent':
            query = '''
                         SELECT DISTINCT ?item ?music WHERE {
                        {
                         SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                                   
                                        ?item wdt:P31 wd:Q134556 ;
                                              wdt:P1476 ?music;
                                              wdt:P577 ?date 
                                        FILTER(BOUND(?date) && DATATYPE(?date) = xsd:dateTime).
                                        BIND(NOW() - ?date AS ?distance).
                                        FILTER(0 <= ?distance && ?distance < 31)                    
                         }
                        }         
                    '''
            r = requests.get(url, params={'format': 'json', 'query': query})
            data = r.json()
            for e in data['results']['bindings']:
                music[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['music']['value']

        if request.GET['Order'] == '80':
            query = '''
                          SELECT DISTINCT ?item ?music WHERE {
                           SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                        
                           ?item wdt:P31 wd:Q134556 ;
                                 wdt:P1476 ?music;
                                 wdt:P577 ?date.
                        
                                 FILTER((?date >= "1980-01-01T00:00:00Z"^^xsd:dateTime) && (?date <= "1989-12-31T00:00:00Z"^^xsd:dateTime))
                                                                  
                        }        
                    '''
            r = requests.get(url, params={'format': 'json', 'query': query})
            data = r.json()
            for e in data['results']['bindings']:
                music[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['music']['value']

        if request.GET['Order'] == '90':
            query = '''
                           SELECT DISTINCT ?item ?music WHERE {
                          {
                            SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                               
                            ?item wdt:P31 wd:Q134556 ;
                                  wdt:P1476 ?music;
                                  wdt:P577 ?date.
                            
                           FILTER((?date >= "1990-01-01T00:00:00Z"^^xsd:dateTime) && (?date <= "1999-12-31T00:00:00Z"^^xsd:dateTime))
                                          
                          }
                        }        
                    '''
            r = requests.get(url, params={'format': 'json', 'query': query})
            data = r.json()
            for e in data['results']['bindings']:
                music[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['music']['value']

        if request.GET['Order'] == '2000':
            query = '''
                        SELECT DISTINCT ?item ?music WHERE {
                              {
                                     SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                                               
                                                    ?item wdt:P31 wd:Q134556 ;
                                                          wdt:P1476 ?music;
                                                          wdt:P577 ?date.
                                                    
                                                   FILTER((?date >= "2000-01-01T00:00:00Z"^^xsd:dateTime) && (?date <= "2009-12-31T00:00:00Z"^^xsd:dateTime))
                                                          
                              }
                        }      
                    '''
            r = requests.get(url, params={'format': 'json', 'query': query})
            data = r.json()
            for e in data['results']['bindings']:
                music[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['music']['value']

        if request.GET['Order'] == '2010':
            query = '''
                        SELECT DISTINCT ?item ?music WHERE {
                              
                         SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                                   
                                        ?item wdt:P31 wd:Q134556 ;
                                              wdt:P1476 ?music;
                                              wdt:P577 ?date.
                                        
                                       FILTER((?date >= "2010-01-01T00:00:00Z"^^xsd:dateTime) && (?date <= "2019-12-31T00:00:00Z"^^xsd:dateTime))
                                                          
                              
                        }      
                    '''
            r = requests.get(url, params={'format': 'json', 'query': query})
            data = r.json()
            for e in data['results']['bindings']:
                music[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['music']['value']

    else:
        query = '''
                  SELECT DISTINCT ?item ?music ?name WHERE {
                 {
                               SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                               ?item wdt:P31 wd:Q134556 ;
                                     wdt:P1476 ?music;
                                     wdt:P175 ?artist.
                               ?artist wdt:P1477 ?name.
                 }
                 UNION
                 {
                       SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                               ?item wdt:P31 wd:Q134556 ;
                                     wdt:P1476 ?music;
                                     wdt:P175 ?artist.
                               ?artist wdt:P1448 ?name
                 }

                }

              '''
        r = requests.get(url, params={'format': 'json', 'query': query})
        data = r.json()
        for e in data['results']['bindings']:
            music[(e['item']['value']).replace("http://www.wikidata.org/entity/", "")] = e['music']['value']

    tparams = {
        'music': music,
        'artist': name,

    }
    return render(request, 'music.html', tparams)

def detailsAlbum(request):
    image = ""
    wiki_artist = ""
    genre = []
    date = []
    record = []
    playlist = []
    image = []
    musics = []
    artists_names = []

    if "Album" in request.GET:

        endpoint = "http://localhost:7200"
        repo_name = "Musica"
        client = ApiClient(endpoint=endpoint)
        accessor = GraphDBApi(client)

        query = '''
                    PREFIX mus:<http://www.music.pt/preds/>
                    SELECT DISTINCT ?wiki_artist ?album ?artist
                    WHERE{
                        ?artist_id mus:album <''' + request.GET['Album']+ '''>.
                        OPTIONAL{?artist_id mus:artist ?artist.}
                        OPTIONAL{?artist_id mus:wikidataid ?wiki_artist}
                        <''' + request.GET['Album']+ '''> mus:name ?album.
                    }
                    limit 1
              '''

        payload_query = {"query": query}
        res = accessor.sparql_select(body=payload_query,
                                     repo_name=repo_name)
        res = json.loads(res)
        for e in res['results']['bindings']:
            if 'wiki_artist' in e:
                wiki_artist = e['wiki_artist']['value']
            album = e['album']['value']
            artists_names.append(e['artist']['value'])
        if wiki_artist== "":
            wiki_artist = insertBD(artists_names[0])
        url = 'https://query.wikidata.org/sparql'
        query = '''
                       SELECT DISTINCT ?date ?itemLabel ?genreLabel ?recordLabel ?playlistLabel ?musicsLabel ?musics2Label ?image ?artistLabel WHERE {
                      SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                      ?item wdt:P175 wd:''' + wiki_artist.replace("http://www.wikidata.org/entity/", "")+ ''';
                            ?label "'''+album +'''"@en.
                      OPTIONAL{?item wdt:P136 ?genre}
                      OPTIONAL{?item wdt:P577 ?date}
                      OPTIONAL{?item wdt:P264 ?record}
                      OPTIONAL {?item wdt:P175 ?artist }
                      OPTIONAL{?item wdt:P4300 ?playlist}
                      OPTIONAL{?item wdt:P154 ?image}
                      OPTIONAL{?item wdt:P658 ?musics}
                      OPTIONAL{?item wdt:P527 ?musics2}
                     }
                   '''
        r = requests.get(url, params={'format': 'json', 'query': query})
        data = r.json()
        for e in data['results']['bindings']:
            if 'genreLabel' in e:
                if e['genreLabel']['value'] not in genre:
                    genre.append(e['genreLabel']['value'])
            if 'date' in e:
                date = e['date']['value'].replace("T00:00:00Z", "")
            if 'recordLabel' in e:
                if e['recordLabel']['value'] not in record:
                    record.append(e['recordLabel']['value'])
            if 'playlistLabel' in e:
                if e['playlistLabel']['value'] not in playlist:
                    playlist.append(e['playlistLabel']['value'])
            if 'musicLabel' in e:
                if e['musicLabel']['value'] not in musics:
                    musics.append(e['musicLabel']['value'])
            if 'musics2Label' in e:
                if e['musics2Label']['value'] not in musics:
                    musics.append(e['musics2Label']['value'])
            if 'image' in e:
                image = e['image']['value']
            if 'artistLabel' in e:
                if e['artistLabel']['value'] not in artists_names:
                    artists_names.append(e['artistsLabel']['value'])

        if musics == []:
            query= '''
            SELECT DISTINCT ?item ?itemLabel WHERE {

            SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
            ?item wdt:P31 wd:Q134556 ;
                  wdt:P175 wd:''' + wiki_artist.replace("http://www.wikidata.org/entity/", "") + '''.
            }
            '''
            r = requests.get(url, params={'format': 'json', 'query': query})
            data = r.json()
            for e in data['results']['bindings']:
                musics.append(e['itemLabel']['value'])

        if musics == []:
            endpoint = "http://localhost:7200"
            repo_name = "Musica"
            client = ApiClient(endpoint=endpoint)
            accessor = GraphDBApi(client)

            query = '''
                PREFIX mus:<http://www.music.pt/preds/>
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                SELECT DISTINCT ?album 
                WHERE{
                   ?artist_id mus:wikidataid "'''+ wiki_artist.replace("http://www.wikidata.org/entity/", "") +'''"^^xsd:integer;
                               mus:album ?album_id.        
                   ?album_id mus:name ?album.
                }
                    '''
            payload_query = {"query": query}
            res = accessor.sparql_select(body=payload_query,
                                         repo_name=repo_name)
            res = json.loads(res)
            for e in res['results']['bindings']:
               musics.append(e['album']['value'])


        tparams = {
            'album': album,
            'artists_name': artists_names,
            'genre' : genre,
            'date' : date,
            'record' : record,
            'playlist' : playlist,
            'image' : image,
            'musics' : musics,
            'songs': len(musics),

        }

    return render(request, 'album_details.html', tparams)

def detailsAlbum2(request):
    album =""
    image = ""
    wiki_artist = ""
    genre = []
    date = []
    record = []
    playlist = []
    image = []
    musics = []
    artists_names = []
    url = 'https://query.wikidata.org/sparql'
    if "Album" in request.GET:
        query = ''' 
            SELECT DISTINCT ?itemLabel ?genreLabel ?artistLabel ?tracklistLabel ?date ?recordLabel ?image WHERE {
              SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }

                ?item wdt:P31 wd:Q482994;
                filter(?item= wd:'''+ request.GET['Album'].replace("http://www.wikidata.org/entity/", "")+ ''')
                OPTIONAL {?item wdt:P136 ?genre }
                OPTIONAL{?item wdt:P577 ?date}
                OPTIONAL{?item wdt:P264 ?record}
                OPTIONAL {?item wdt:P175 ?artist }
                OPTIONAL{?item wdt:P154 ?image}
                OPTIONAL {?item wdt:P658 ?tracklist }
                OPTIONAL{?item wdt:P4300 ?tracklist}
                OPTIONAL{?item wdt:P527 ?tracklist}

            }
        '''
        r = requests.get(url, params={'format': 'json', 'query': query})
        data = r.json()
        for e in data['results']['bindings']:
            if 'itemLabel' in e:
                album = e['itemLabel']['value']
            if 'genreLabel' in e:
                if e['genreLabel']['value'] not in genre:
                    genre.append(e['genreLabel']['value'])
            if 'date' in e:
                date = e['date']['value'].replace("T00:00:00Z", "")
            if 'recordLabel' in e:
                if e['recordLabel']['value'] not in record:
                    record.append(e['recordLabel']['value'])
            if 'tracklistLabel' in e:
                if e['tracklistLabel']['value'] not in playlist:
                    musics.append(e['tracklistLabel']['value'])
            if 'image' in e:
                image = e['image']['value']
            if 'artistLabel' in e:
                if e['artistLabel']['value'] not in artists_names:
                    artists_names.append(e['artistLabel']['value'])

            tparams = {
                'album': album,
                'artists_name': artists_names,
                'genre': genre,
                'date': date,
                'record': record,
                'image': image,
                'musics': musics,
                'songs': len(musics),
            }
    return render(request, 'album_details.html', tparams)

def detailsMusic(request):
    youtube = ""
    lyrics = ""
    date = ""
    artist = []
    record = []
    genre = []
    url = 'https://query.wikidata.org/sparql'
    if 'Music' in request.GET:
        query = '''
                  SELECT DISTINCT ?item ?itemLabel ?artistLabel ?genreLabel ?date ?recordLabel ?youtube_videoID ?lyrics  WHERE {
                  
                         SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                         {
                              ?item wdt:P31 wd:Q134556 .
                              Filter (?item = wd:''' + request.GET['Music'] +''')   
                              OPTIONAL {?item wdt:P175 ?artist  }
                              OPTIONAL {?item wdt:P136 ?genre }
                              OPTIONAL {?item wdt:P264 ?record }
                              OPTIONAL {?item wdt:P577 ?date }
                              OPTIONAL {?item wdt:P1651 ?youtube_videoID }
                              OPTIONAL {?item wdt:P2624 ?lyrics }
                        }
                        UNION
                        {
                              ?item wdt:P31 wd:Q7366 .
                              Filter (?item = wd:''' + request.GET['Music'] +''')   
                              OPTIONAL {?item wdt:P175 ?artist  }
                              OPTIONAL {?item wdt:P136 ?genre }
                              OPTIONAL {?item wdt:P264 ?record }
                              OPTIONAL {?item wdt:P577 ?date }
                              OPTIONAL {?item wdt:P1651 ?youtube_videoID }
                              OPTIONAL {?item wdt:P2624 ?lyrics }
                        }
    
                  }
                '''
        r = requests.get(url, params={'format': 'json', 'query': query})
        data = r.json()

        for e in data['results']['bindings']:
            musica = e['itemLabel']['value']
            wiki = e['item']['value']

            if 'artistLabel' in e:
                if e['artistLabel']['value'] not in artist:
                    artist.append(e['artistLabel']['value'])
            if 'recordLabel' in e:
                if e['recordLabel']['value'] not in record:
                    record.append(e['recordLabel']['value'])
            if 'genreLabel' in e:
                if e['genreLabel']['value'] not in genre:
                    genre.append(e['genreLabel']['value'])
            if 'youtube_videoID' in e:
                youtube = e['youtube_videoID']['value']
            if 'lyrics' in e:
                lyrics = e['lyrics']['value']
            if 'date' in e:
                date = e['date']['value'].replace("T00:00:00Z", "")

        tparams = {
            'music': musica,
            'artists': artist,
            'genre': genre,
            'record': record,
            'youtube':youtube,
            'lyrics':lyrics,
            'date':date,
            'wiki':wiki.replace("http://www.wikidata.org/entity/", "")
        }
    return render(request, "music_details.html", tparams)

def detailsGenres(request):
    endpoint = "http://localhost:7200"
    repo_name = "Musica"
    client = ApiClient(endpoint=endpoint)
    accessor = GraphDBApi(client)
    query = """
              
        """
    tparams = {

    }
    return render(request, 'genre_details.html', tparams)

def detailsGenres2(request):
    image = ""
    genre = ""
    date = ""
    albums = dict()
    artists = dict()
    music = []
    country = ""
    url = 'https://query.wikidata.org/sparql'
    if 'Genre' in request.GET:
        query = '''
                SELECT DISTINCT ?genre ?albunsLabel ?albuns_image ?artist ?artistLabel ?musicLabel ?image ?countryLabel ?inceptionLabel ?artist_image WHERE {
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                {
                   ?albuns wdt:P31 wd:Q482994;
                           wdt:P136 wd:'''+ request.GET['Genre'].replace("http://www.wikidata.org/entity/", "")+ '''.
                    OPTIONAL{?albuns wdt:P18 ?albuns_image}

                 }
                 UNION
                 {
                   ?artist wdt:P31 wd:Q215380;
                           wdt:P136 wd:'''+ request.GET['Genre'].replace("http://www.wikidata.org/entity/", "")+ ''';
                           wdt:P18 ?artist_image.
                   }
                   
                UNION
                 {
                   
                   ?artist wdt:P31 wd:Q5;
                           wdt:P136 wd:'''+ request.GET['Genre'].replace("http://www.wikidata.org/entity/", "")+ ''';
                           wdt:P18 ?artist_image.
                   }
                  UNION
                   {
                     ?music wdt:P31 wd:Q134556;
                           wdt:P136 wd:'''+ request.GET['Genre'].replace("http://www.wikidata.org/entity/", "")+ ''';

                    }
                   UNION
                   {
                     wd:'''+ request.GET['Genre'].replace("http://www.wikidata.org/entity/", "")+ ''' rdfs:label ?genre.
                     FILTER (langMatches( lang(?genre), "EN" ) ) 
                     OPTIONAL{wd:'''+ request.GET['Genre'].replace("http://www.wikidata.org/entity/", "")+ ''' wdt:P18 ?image}
                     OPTIONAL{wd:'''+ request.GET['Genre'].replace("http://www.wikidata.org/entity/", "")+ ''' wdt:P495 ?country}
                      OPTIONAL{wd:'''+ request.GET['Genre'].replace("http://www.wikidata.org/entity/", "")+ ''' wdt:P571 ?inception}
                     }
                }
               '''
        r = requests.get(url, params={'format': 'json', 'query': query})
        data = r.json()
        for e in data['results']['bindings']:
            if 'albunsLabel' in e:
                if 'albuns_image' not in e:
                    albums[e['albunsLabel']['value']] = ""
                else:
                    albums[e['albunsLabel']['value']] = e['albuns_image']['value']
            if 'artist' in e:
                artists[e['artistLabel']['value']] = e['artist_image']['value']
            if 'musicLabel' in e:
                music.append(e['musicLabel']['value'])
            if 'image' in e:
                image = e['image']['value']
            if 'countryLabel' in e:
                country= e['countryLabel']['value']
            if 'inceptionLabel' in e:
                date = (e['inceptionLabel']['value']).replace("T00:00:00Z", "")
            if 'genre' in e:
                genre = e['genre']['value']


    tparams = {
        'albums' : {A: N for (A, N) in [x for x in albums.items()][:9]},
        'artists': {A: N for (A, N) in [x for x in artists.items()][:9]},
        'music': music[:6],
        'image':image,
        'country':country,
        'date':date,
        'genre': genre,
        'nartists':len(artists),
        'nmusic': len(music),
        'nalbums': len(albums)

    }
    return render(request, 'genre_details.html', tparams)

def detailsArtists(request):
    genre = []
    albums = dict()
    musics = []
    artist_photo = ""

    artist = ""
    date = ""
    country = ""

    url = 'https://query.wikidata.org/sparql'
    if 'Artist' in request.GET:
        query = '''
            SELECT DISTINCT  ?item ?itemLabel ?photo ?genreLabel ?albumsLabel ?musicsLabel ?image ?date ?countryLabel  WHERE {
              SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
              {
                ?musics wdt:P31 wd:Q134556 ;
                      wdt:P175 wd:'''+request.GET['Artist'].replace("http://www.wikidata.org/entity/", "")+'''
              }
              UNION
              {
                ?albums wdt:P175 wd:'''+request.GET['Artist'].replace("http://www.wikidata.org/entity/", "")+''';
                        wdt:P31 wd:Q482994.
                 OPTIONAL{?albums wdt:P18 ?image}
                        
              } 
              UNION
              {
                OPTIONAL{?item wdt:P31 wd:Q215380;}
                OPTIONAL{?item wdt:P31 wd:Q5}
                filter(?item=wd:'''+request.GET['Artist'].replace("http://www.wikidata.org/entity/", "")+''')
                OPTIONAL{?item wdt:P18 ?photo}
                OPTIONAL{?item wdt:P136 ?genre}
                OPTIONAL{?item wdt:P2031 ?date}
                OPTIONAL{?item wdt:P495 ?country}
              }
     
            }
        '''
        r = requests.get(url, params={'format': 'json', 'query': query})
        data = r.json()
        for e in data['results']['bindings']:
            if 'itemLabel' in e:
                artist = e['itemLabel']['value']

            if 'photo' in e:
               artist_photo = e['photo']['value']
            if 'genreLabel' in e:
                if e['genreLabel']['value'] not in genre:
                    genre.append(e['genreLabel']['value'])
            if 'albumsLabel' in e:
                if e['albumsLabel']['value'] not in albums:
                    if 'image' not in e:
                        albums[e['albumsLabel']['value']] = ""
                    else:
                        albums[e['albumsLabel']['value']] = e['image']['value']
            if 'musicsLabel' in e:
                if e['musicsLabel']['value'] not in musics:
                    musics.append(e['musicsLabel']['value'])
            if 'date' in e:
                date = e['date']['value'].replace("T00:00:00Z", "")
            if 'countryLabel' in e:
                country = e['countryLabel']['value']
        tparams = {
            'date':date,
            'country': country,
            'musics': musics,
            'genre': genre,
            'photo2': artist_photo,
            'albums': albums,
            'artist':artist
        }
    return render(request, 'artist_details.html', tparams)

def playlist(request):
    nome = ""
    a_photo = dict()
    g_photo = dict()
    al_photo = dict()
    music = dict()
    artist = dict()
    genre = dict()
    album = dict()
    endpoint = "http://localhost:7200"
    repo_name = "Musica"
    client = ApiClient(endpoint=endpoint)
    accessor = GraphDBApi(client)
    # MUSICAS
    query = """
                PREFIX mus:<http://www.music.pt/preds/>
                PREFIX id:<http://www.music.pt/id/>
                SELECT DISTINCT?playlist ?music ?wiki where
                {
                   id:playlist mus:playlist ?playlist;
                               mus:music ?music_id.
                    ?music_id mus:name ?music;
                              mus:wikidataid ?wiki.
                }
            """
    payload_query = {"query": query}
    res = accessor.sparql_select(body=payload_query,
                                 repo_name=repo_name)
    res = json.loads(res)
    for e in res['results']['bindings']:
        if 'wiki' in e and 'music' in e:
            music[e['wiki']['value']] = e['music']['value']
        if 'playlist' in e:
            nome = e['playlist']['value']
    # GENEROS
    query = '''
                PREFIX mus:<http://www.music.pt/preds/>
                PREFIX id:<http://www.music.pt/id/>
                PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>
                
                SELECT ?genre ?wiki ?photo 
                WHERE{
                    id:playlist mus:genre ?genre_id.
                    ?genre_id mus:genre_name ?genre;
                              mus:wikidataid ?wiki;
                              mus:photo ?photo.
                }
            '''
    payload_query = {"query": query}
    res = accessor.sparql_select(body=payload_query,
                                 repo_name=repo_name)
    res = json.loads(res)
    for e in res['results']['bindings']:
        if 'wiki' in e and 'genre' in e:
            genre[e['wiki']['value']] = e['genre']['value']
        if 'photo' in e:
            g_photo[e['wiki']['value']] = e['photo']['value']

    # ARTISTAS
    query = '''
                    PREFIX mus:<http://www.music.pt/preds/>
                    PREFIX id:<http://www.music.pt/id/>
                    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                    SELECT DISTINCT ?artist_id ?artist ?photo ?wiki
                    WHERE{
                        id:playlist mus:p_artist ?artist_id.
                        ?artist_id mus:artist ?artist;
                                   mus:wikidataid ?wiki;
                                   mus:photo ?photo.
                    }
                '''
    payload_query = {"query": query}
    res = accessor.sparql_select(body=payload_query,
                                 repo_name=repo_name)
    res = json.loads(res)
    for e in res['results']['bindings']:
        if 'wiki' in e and 'artist' in e:
            artist[e['wiki']['value']] = e['artist']['value']
        if 'photo' in e:
            a_photo[e['wiki']['value']] = e['photo']['value']

    # ALBUMS
    query = '''
                PREFIX mus:<http://www.music.pt/preds/>
                PREFIX id:<http://www.music.pt/id/>
                PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>
                
                SELECT ?album ?wiki ?photo WHERE
                {
                    id:playlist mus:album ?album_id.
                    ?album_id mus:name ?album;
                            mus:photo ?photo;
                            mus:wikidataid ?wiki.
                }
            '''
    payload_query = {"query": query}
    res = accessor.sparql_select(body=payload_query,
                                 repo_name=repo_name)
    res = json.loads(res)
    for e in res['results']['bindings']:
        if 'wiki' in e and 'album' in e:
            album[e['wiki']['value']] = e['album']['value']
        if 'photo' in e:
            al_photo[e['wiki']['value']] = e['photo']['value']

    tparams = {
        'music': music,
        'nome': nome,
        'songs': len(music),
        'a_photo':a_photo,
        'artist':artist,
        'cartist':len(artist),
        'genres':genre,
        'cgenres': len(genre),
        'g_photo':g_photo,
        'album':album,
        'calbum': len(album),
        'al_photo':al_photo

    }
    return render(request, 'playlist.html', tparams)

def addMusic(request):
    musica = ""
    if 'Music' in request.GET:
        url = 'https://query.wikidata.org/sparql'
        query = '''
                       SELECT DISTINCT ?itemLabel WHERE {

                         SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                          
                          ?item wdt:P31 wd:Q134556 . 
                          Filter (?item = wd:'''+request.GET['Music']+''')
                          OPTIONAL {?item wdt:P175 ?artist  }
                          
                  }
                   '''

        r = requests.get(url, params={'format': 'json', 'query': query})
        data = r.json()

        for e in data['results']['bindings']:
            if 'itemLabel' in e:
                musica = e['itemLabel']['value']

        # Inserir na nossa base de dados
        endpoint = "http://localhost:7200"
        repo_name = "Musica"
        client = ApiClient(endpoint=endpoint)
        accessor = GraphDBApi(client)
        update = '''
                PREFIX mus:<http://www.music.pt/preds/>
                PREFIX id:<http://www.music.pt/id/>
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                INSERT DATA
                {
                    id:playlist mus:music <http://www.music.pt/music/'''+(musica.lower()).replace(" ", "_")+'''>.
                    <http://www.music.pt/music/'''+(musica.lower()).replace(" ", "_")+'''> mus:name "'''+musica+'''";
                                                                                          mus:wikidataid "'''+request.GET['Music']+'''"^^xsd:integer.
                }
            '''

        payload_query = {"update": update}
        res = accessor.sparql_update(body=payload_query,
                                     repo_name=repo_name)

        response = redirect("/playlist/")
        return response

def addArtist(request):
    photo = ""
    artist = ""
    if 'Artist' in request.GET:
        url = 'https://query.wikidata.org/sparql'
        query = '''
                SELECT DISTINCT ?item ?itemLabel ?photo WHERE {
                  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                  {
                    ?item wdt:P31 wd:Q215380;
                    filter(?item=wd:'''+request.GET['Artist'].replace("http://www.wikidata.org/entity/", "")+''')
                    OPTIONAL{?item wdt:P18 ?photo}
                    OPTIONAL{?item wdt:P154 ?logo}
                  }
                  UNION
                  {
                    ?item wdt:P31 wd:Q5;
                    filter(?item=wd:'''+request.GET['Artist'].replace("http://www.wikidata.org/entity/", "")+''')
                    OPTIONAL{?item wdt:P18 ?photo}
                    OPTIONAL{?item wdt:P154 ?photo}
                  }
                }
                   '''
        r = requests.get(url, params={'format': 'json', 'query': query})
        data = r.json()

        for e in data['results']['bindings']:
            if 'itemLabel' in e and 'item' in e:
                artist = e['itemLabel']['value']
            if 'photo' in e:
                photo = e['photo']['value']


        # Inserir na nossa base de dados
        endpoint = "http://localhost:7200"
        repo_name = "Musica"
        client = ApiClient(endpoint=endpoint)
        accessor = GraphDBApi(client)
        update = '''
                PREFIX mus:<http://www.music.pt/preds/>
                PREFIX id:<http://www.music.pt/id/>
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                INSERT DATA
                {
                    id:playlist mus:p_artist <http://www.music.pt/id/'''+(artist.lower()).replace(" ", "_")+'''>.
                    <http://www.music.pt/id/'''+(artist.lower()).replace(" ", "_")+'''> mus:photo "'''+photo+'''";
                             mus:wikidataid "'''+request.GET['Artist'].replace("http://www.wikidata.org/entity/", "")+'''"^^xsd:integer;
                             mus:artist "'''+artist+'''".
                }
            '''

        payload_query = {"update": update}
        res = accessor.sparql_update(body=payload_query,
                                     repo_name=repo_name)

        response = redirect("/playlist/")
        return response

def addGenre(request):
    photo = ""
    genre = ""
    if 'Genre' in request.GET:
        url = 'https://query.wikidata.org/sparql'
        query = '''
                SELECT DISTINCT ?image ?nome WHERE {
                  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                  wd:'''+ request.GET['Genre'].replace("http://www.wikidata.org/entity/", "")+ ''' rdfs:label ?nome.
                   FILTER (langMatches( lang(?nome), "EN" ) ) 
                  OPTIONAL{wd:'''+request.GET['Genre'].replace("http://www.wikidata.org/entity/", "")+''' wdt:P18 ?image}
                              
                }
                '''
        r = requests.get(url, params={'format': 'json', 'query': query})
        data = r.json()

        for e in data['results']['bindings']:
            if 'nome' in e:
                genre = e['nome']['value']
            if 'image' in e:
                photo = e['image']['value']

        # Inserir na nossa base de dados
        endpoint = "http://localhost:7200"
        repo_name = "Musica"
        client = ApiClient(endpoint=endpoint)
        accessor = GraphDBApi(client)
        update = '''
                PREFIX mus:<http://www.music.pt/preds/>
                PREFIX id:<http://www.music.pt/id/>
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                INSERT DATA
                {
                    id:playlist mus:genre <http://www.music.pt/genre/'''+(genre.lower()).replace(" ", "_")+'''>.
                    <http://www.music.pt/genre/'''+(genre.lower()).replace(" ", "_")+'''> mus:photo "'''+photo+'''";
                             mus:wikidataid "'''+request.GET['Genre'].replace("http://www.wikidata.org/entity/", "")+'''"^^xsd:integer;
                             mus:genre_name "'''+genre+'''".
                }
            '''

        payload_query = {"update": update}
        res = accessor.sparql_update(body=payload_query,
                                     repo_name=repo_name)

    if 'Genre2' in request.GET:
        wiki_genre = ""

        url = 'https://query.wikidata.org/sparql'
        query = '''
              SELECT DISTINCT ?photo ?item ?itemLabel WHERE {
              SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
              
              ?item wdt:P31 wd:Q188451;
                    ?Label "'''+(request.GET['Genre2']).replace("http://www.music.pt/id/", "").replace("_", " ")+'''"@en.
              OPTIONAL{?item wdt:P18 ?photo}
            }
        '''
        r = requests.get(url, params={'format': 'json', 'query': query})
        data = r.json()

        for e in data['results']['bindings']:
            if 'itemLabel' in e:
                genre = e['itemLabel']['value']
            if 'photo' in e:
                photo = e['photo']['value']
            if 'item' in e:
                wiki_genre = e['item']['value']

        endpoint = "http://localhost:7200"
        repo_name = "Musica"
        client = ApiClient(endpoint=endpoint)
        accessor = GraphDBApi(client)
        update = '''
                        PREFIX mus:<http://www.music.pt/preds/>
                        PREFIX id:<http://www.music.pt/id/>
                        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                        INSERT DATA
                        {
                            id:playlist mus:genre <http://www.music.pt/genre/''' + (genre.lower()).replace(" ", "_") + '''>.
                            <http://www.music.pt/genre/''' + (genre.lower()).replace(" ",
                                                                                     "_") + '''> mus:photo "''' + photo + '''";
                                     mus:wikidataid "''' + wiki_genre.replace(
            "http://www.wikidata.org/entity/", "") + '''"^^xsd:integer;
                                     mus:genre_name "''' + genre + '''".
                        }
                    '''
        payload_query = {"update": update}
        res = accessor.sparql_update(body=payload_query,
                                     repo_name=repo_name)

    response = redirect("/playlist/")
    return response

def addAlbum(request):
    photo = ""
    album = ""

    if 'Album' in request.GET:
        url = 'https://query.wikidata.org/sparql'
        query = '''
                 SELECT DISTINCT ?item ?itemLabel ?image WHERE {
                  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                  ?item wdt:P31 wd:Q482994;
                  filter(?item=wd:''' + request.GET['Album'].replace("http://www.wikidata.org/entity/", "") + ''')
                  OPTIONAL{?item wdt:P154 ?image}
                  OPTIONAL{?item wdt:P18 ?image}            
                }
                '''
        r = requests.get(url, params={'format': 'json', 'query': query})
        data = r.json()

        for e in data['results']['bindings']:
            if 'itemLabel' in e:
                album = e['itemLabel']['value']
            if 'image' in e:
                photo = e['image']['value']

        # Inserir na nossa base de dados
        endpoint = "http://localhost:7200"
        repo_name = "Musica"
        client = ApiClient(endpoint=endpoint)
        accessor = GraphDBApi(client)
        update = '''
                    PREFIX mus:<http://www.music.pt/preds/>
                    PREFIX id:<http://www.music.pt/id/>
                    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                    INSERT DATA
                    {
                        id:playlist mus:album <http://www.music.pt/album/''' + (album.lower()).replace(" ", "_") + '''>.
                        <http://www.music.pt/album/''' + (album.lower()).replace(" ", "_") + '''> mus:name "''' + album + '''";
                                                         mus:wikidataid "''' + request.GET['Album'].replace("http://www.wikidata.org/entity/", "") + '''"^^xsd:integer;
                                                         mus:photo "''' + photo + '''".
                    }
            '''
        payload_query = {"update": update}
        res = accessor.sparql_update(body=payload_query,
                                     repo_name=repo_name)

    if 'Album2' in request.GET:

        wiki_genre = ""

        url = 'https://query.wikidata.org/sparql'
        query = '''
                 SELECT DISTINCT ?photo ?item ?itemLabel WHERE {
                 SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                 {
                 ?item wdt:P31 wd:Q482994;
                       ?Label "''' + ((request.GET['Album2']).replace("http://www.music.pt/id/", "").replace("_", " ")).capitalize() + '''"@en.
                 OPTIONAL{?item wdt:P18 ?photo}
                 }
                 UNION
                 {
                 ?item wdt:P31 wd:Q208569;
                       ?Label "''' + ((request.GET['Album2']).replace("http://www.music.pt/id/", "").replace("_", " ")).capitalize() + '''"@en.
                 OPTIONAL{?item wdt:P18 ?photo}
                 }
                }
           '''
        r = requests.get(url, params={'format': 'json', 'query': query})
        data = r.json()

        for e in data['results']['bindings']:
            if 'itemLabel' in e:
                album = e['itemLabel']['value']
            if 'photo' in e:
                photo = e['photo']['value']
            if 'item' in e:
                wiki_genre = e['item']['value']

        # Inserir na nossa base de dados
        endpoint = "http://localhost:7200"
        repo_name = "Musica"
        client = ApiClient(endpoint=endpoint)
        accessor = GraphDBApi(client)
        update = '''
                            PREFIX mus:<http://www.music.pt/preds/>
                            PREFIX id:<http://www.music.pt/id/>
                            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                            INSERT DATA
                            {
                                id:playlist mus:album <http://www.music.pt/album/''' + (album.lower()).replace(" ",
                                                                                                               "_") + '''>.
                                <http://www.music.pt/album/''' + (album.lower()).replace(" ",
                                                                                         "_") + '''> mus:name "''' + album + '''";
                                                                 mus:wikidataid "''' + wiki_genre.replace(
            "http://www.wikidata.org/entity/", "") + '''"^^xsd:integer;
                                                                 mus:photo "''' + photo + '''".
                            }
                    '''
        print(update)
        payload_query = {"update": update}
        res = accessor.sparql_update(body=payload_query,
                                     repo_name=repo_name)

        payload_query = {"update": update}
        res = accessor.sparql_update(body=payload_query,
                                     repo_name=repo_name)
    response = redirect("/playlist/")
    return response

def removeMusic(request):
    musica = ""
    if 'Music' in request.GET:
        url = 'https://query.wikidata.org/sparql'
        query = '''
                   SELECT DISTINCT ?itemLabel WHERE {

                     SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                      {
                      ?item wdt:P31 wd:Q134556 . 
                      Filter (?item = wd:''' + request.GET['Music'] + ''')
                      OPTIONAL {?item wdt:P175 ?artist  }
                      }
                      UNION
                      {
                         ?item wdt:P31 wd:Q7366 . 
                         Filter (?item = wd:''' + request.GET['Music'] + ''')
                         OPTIONAL {?item wdt:P175 ?artist  }
                      }  
                    }
               '''
        r = requests.get(url, params={'format': 'json', 'query': query})
        data = r.json()

        for e in data['results']['bindings']:
            if 'itemLabel' in e:
                musica = e['itemLabel']['value']

        # Remover na nossa base de dados
        endpoint = "http://localhost:7200"
        repo_name = "Musica"
        client = ApiClient(endpoint=endpoint)
        accessor = GraphDBApi(client)
        update = '''
                    PREFIX mus:<http://www.music.pt/preds/>
                    PREFIX id:<http://www.music.pt/id/>
                    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                    DELETE DATA
                    {
                        id:playlist mus:music <http://www.music.pt/music/''' + (musica.lower()).replace(" ", "_") + '''>.
                        <http://www.music.pt/music/''' + (musica.lower()).replace(" ",
                                                                                  "_") + '''> mus:name "''' + musica + '''";
                                                                                              mus:wikidataid "''' + \
                 request.GET['Music'] + '''"^^xsd:integer.
                    }
                '''
        print(update)
        payload_query = {"update": update}
        res = accessor.sparql_update(body=payload_query,
                                     repo_name=repo_name)
        response = redirect("/playlist/")
        return response

def removeArtist(request):
    photo = ""
    artist = ""
    if 'Artist' in request.GET:
        url = 'https://query.wikidata.org/sparql'
        query = '''
                SELECT DISTINCT ?item ?itemLabel ?photo WHERE {
                  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                  {
                    ?item wdt:P31 wd:Q215380;
                    filter(?item=wd:'''+request.GET['Artist'].replace("http://www.wikidata.org/entity/", "")+''')
                    OPTIONAL{?item wdt:P18 ?photo}
                    OPTIONAL{?item wdt:P154 ?logo}
                  }
                  UNION
                  {
                    ?item wdt:P31 wd:Q5;
                    filter(?item=wd:'''+request.GET['Artist'].replace("http://www.wikidata.org/entity/", "")+''')
                    OPTIONAL{?item wdt:P18 ?photo}
                    OPTIONAL{?item wdt:P154 ?photo}
                  }
                }
                   '''
        r = requests.get(url, params={'format': 'json', 'query': query})
        data = r.json()

        for e in data['results']['bindings']:
            if 'itemLabel' in e and 'item' in e:
                artist = e['itemLabel']['value']
            if 'photo' in e:
                photo = e['photo']['value']


        # Eliminar na nossa base de dados
        endpoint = "http://localhost:7200"
        repo_name = "Musica"
        client = ApiClient(endpoint=endpoint)
        accessor = GraphDBApi(client)
        update = '''
                PREFIX mus:<http://www.music.pt/preds/>
                PREFIX id:<http://www.music.pt/id/>
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                DELETE DATA
                {
                    id:playlist mus:p_artist <http://www.music.pt/id/'''+(artist.lower()).replace(" ", "_")+'''>.
                    <http://www.music.pt/id/'''+(artist.lower()).replace(" ", "_")+'''> mus:photo "'''+photo+'''";
                             mus:wikidataid "'''+request.GET['Artist'].replace("http://www.wikidata.org/entity/", "")+'''"^^xsd:integer;
                             mus:artist "'''+artist+'''".
                }
            '''

        payload_query = {"update": update}
        res = accessor.sparql_update(body=payload_query,
                                     repo_name=repo_name)

        response = redirect("/playlist/")
        return response

def removeGenre(request):
    photo = ""
    genre = ""
    if 'Genre' in request.GET:
        url = 'https://query.wikidata.org/sparql'
        query = '''
                SELECT DISTINCT ?image ?nome WHERE {
                  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                  wd:'''+ request.GET['Genre'].replace("http://www.wikidata.org/entity/", "")+ ''' rdfs:label ?nome.
                   FILTER (langMatches( lang(?nome), "EN" ) ) 
                  OPTIONAL{wd:'''+request.GET['Genre'].replace("http://www.wikidata.org/entity/", "")+''' wdt:P18 ?image}
                              
                }
                '''

        r = requests.get(url, params={'format': 'json', 'query': query})
        data = r.json()

        for e in data['results']['bindings']:
            if 'nome' in e:
                genre = e['nome']['value']
            if 'image' in e:
                photo = e['image']['value']

        # Eliminar na nossa base de dados
        endpoint = "http://localhost:7200"
        repo_name = "Musica"
        client = ApiClient(endpoint=endpoint)
        accessor = GraphDBApi(client)
        update = '''
                PREFIX mus:<http://www.music.pt/preds/>
                PREFIX id:<http://www.music.pt/id/>
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                DELETE DATA
                {
                    id:playlist mus:genre <http://www.music.pt/genre/''' + (genre.lower()).replace(" ", "_") + '''>.
                    <http://www.music.pt/genre/''' + (genre.lower()).replace(" ", "_") + '''> mus:photo "''' + photo + '''";
                             mus:wikidataid "''' + request.GET['Genre'].replace("http://www.wikidata.org/entity/", "") + '''"^^xsd:integer;
                             mus:genre_name "''' + genre + '''".
                }
            '''
        print(update)
        payload_query = {"update": update}
        res = accessor.sparql_update(body=payload_query,
                                     repo_name=repo_name)

        response = redirect("/playlist/")
        return response

def removeAlbum(request):
    photo = ""
    album = ""
    if 'Album' in request.GET:
        url = 'https://query.wikidata.org/sparql'
        query = '''
                 SELECT DISTINCT ?item ?itemLabel ?image WHERE {
                  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                  ?item wdt:P31 wd:Q482994;
                  filter(?item=wd:''' + request.GET['Album'].replace("http://www.wikidata.org/entity/", "") + ''')
                  OPTIONAL{?item wdt:P154 ?image}
                  OPTIONAL{?item wdt:P18 ?image}            
                }
                '''
        r = requests.get(url, params={'format': 'json', 'query': query})
        data = r.json()

        for e in data['results']['bindings']:
            if 'itemLabel' in e:
                album = e['itemLabel']['value']
            if 'image' in e:
                photo = e['image']['value']

        # Eliminar na nossa base de dados
        endpoint = "http://localhost:7200"
        repo_name = "Musica"
        client = ApiClient(endpoint=endpoint)
        accessor = GraphDBApi(client)
        update = '''
                    PREFIX mus:<http://www.music.pt/preds/>
                    PREFIX id:<http://www.music.pt/id/>
                    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                    DELETE DATA
                    {
                        id:playlist mus:album <http://www.music.pt/album/''' + (album.lower()).replace(" ", "_") + '''>.
                        <http://www.music.pt/album/''' + (album.lower()).replace(" ", "_") + '''> mus:name "''' + album + '''";
                                                         mus:wikidataid "''' + request.GET['Album'].replace("http://www.wikidata.org/entity/", "") + '''"^^xsd:integer;
                                                         mus:photo "''' + photo + '''".
                    }
            '''
        payload_query = {"update": update}
        res = accessor.sparql_update(body=payload_query,
                                     repo_name=repo_name)

        response = redirect("/playlist/")
        return response

def insertBD(artist):
    wiki_artist = ""
    url = 'https://query.wikidata.org/sparql'
    query = '''
                  SELECT DISTINCT ?item WHERE {
                  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
                  {
                
                    ?item wdt:P31 wd:Q215380;
                          ?Label "'''+ artist +'''"@en
                }
                  UNION
                  {
                
                    ?item wdt:P31 wd:Q215380;
                         ?Label "'''+ artist +'''"@en
                
                  }
                  UNION
                   {
                     ?item wdt:P31 wd:Q5;
                         ?Label "'''+ artist +'''"@en
                   }
                
                }
         '''

    r = requests.get(url, params={'format': 'json', 'query': query})
    data = r.json()
    for e in data['results']['bindings']:
        wiki_artist = e['item']['value']

    endpoint = "http://localhost:7200"
    repo_name = "Musica"
    client = ApiClient(endpoint=endpoint)
    accessor = GraphDBApi(client)
    update = '''
        PREFIX mus:<http://www.music.pt/preds/>
        PREFIX id:<http://www.music.pt/id/>
        INSERT DATA
        {
            id:'''+ (artist.lower()).replace(" ", "_") +''' mus:wikidataid'''+ wiki_artist +'''^^xsd:integer .
        }
    '''

    payload_query = {"update": update}
    res = accessor.sparql_update(body=payload_query,
                                 repo_name=repo_name)

    return wiki_artist

@register.filter
def get_item(dict, key):
    return dict.get(key)

